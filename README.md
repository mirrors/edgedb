<p align="center">
  <span>
    代码在哪里？查看 <a href="https://gitee.com/edgedb/edgedb/tree/master/">master 分支</a>
  </span>
  <br>
  <a href="https://www.edgedb.com">
    <img src="https://images.gitee.com/uploads/images/2022/0527/050108_ab62e705_7490744.png">
  </a>
</p>

<div align="center">
  <h1>EdgeDB</h1>
  <a href="https://github.com/edgedb/edgedb" rel="nofollow">
    <img src="https://img.shields.io/github/stars/edgedb/edgedb?label=GitHub%20%E6%98%9F%E6%95%B0&logo=github&logoColor=8a9095" alt="星数">
  </a>
  <a href="https://github.com/edgedb/edgedb/actions">
    <img src="https://img.shields.io/github/workflow/status/edgedb/edgedb/Tests?event=push&label=%E8%87%AA%E5%8A%A8%E6%B5%8B%E8%AF%95&logo=github&branch=master&logoColor=8a9095" />
  </a>
  <a href="https://github.com/edgedb/edgedb/blob/master/LICENSE">
    <img alt="license" src="https://img.shields.io/github/license/edgedb/edgedb?label=%E5%BC%80%E6%BA%90%E8%AE%B8%E5%8F%AF&logo=OpenSourceInitiative&logoColor=8a9095" />
  </a>
  <a href="https://discord.gg/umUueND6ag">
    <img alt="discord" src="https://img.shields.io/discord/841451783728529451?color=5865F2&label=%E5%9C%A8%E7%BA%BF%E8%81%8A%E5%A4%A9&logo=discord&logoColor=8a9095">
  </a>
  <br />
  <br />
  <a href="https://gitee.com/edgedb/devrel/issues">中文讨论区</a>
  <span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
  <a href="https://space.bilibili.com/518676802/channel/series">视频</a>
  <span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
  <a href="https://www.edgedb.com/easy-edgedb/zh">《EdgeDB 易经》</a>
  <span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
  <a href="https://my.oschina.net/edgedb">OSCHINA</a>
  <span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
  <a href="https://juejin.cn/column/7056894490887323662">掘金专栏</a>
  <span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
  <a href="https://www.zhihu.com/column/c_1470175780085428224">知乎专栏</a>
  <br />
  <br />
  <a href="https://www.edgedb.com/docs/guides/quickstart">快速上手</a>
  <span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
  <a href="https://www.edgedb.com">官方网站</a>
  <span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
  <a href="https://www.edgedb.com/docs">文档</a>
  <span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
  <a href="https://www.edgedb.com/tutorial">交互式教程</a>
  <span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
  <a href="https://www.edgedb.com/blog">博客</a>
  <span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
  <a href="https://discord.gg/umUueND6ag">在线聊天</a>
  <span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
  <a href="https://twitter.com/edgedatabase">推特</a>
  <br />

</div>

<br />
<br />

<br/>
<div align="center">
  <h2>EdgeDB 是什么？</h2>
  <p style="max-width: 450px;">
    EdgeDB 是一种全新的数据库，
    <br/>
    集关系数据库、图数据库和 ORM 的长处于一身，
    <br/>
    我们叫它<b>图-关系型数据库</b>。
  </p>
</div>

<br/>

<br/>
<div align="center">
  <h3>🧩 无表式类型定义 🧩</h3>
</div>
<br/>

作为应用程序的根基，数据库建模（schema）要能轻松书写出来才好，且代码应通俗易懂。

不要再用外键了，表格数据建模是旧时代的产物，与现代编程语言[并不相配](
https://en.wikipedia.org/wiki/Object%E2%80%93relational_impedance_mismatch
)。与之相反，EdgeDB 的 schema 完全符合编程的思路：你可以定义**对象**及其**属性**，并用**链接**来关联其它对象。

```
type 人物 {
  required property 姓名 -> str;
}

type 电影 {
  required property 标题 -> str;
  multi link 演员 -> 人物;
}
```

虽然这个例子很简单，但实际上 EdgeDB
可以支持几乎所有数据库应有的功能：严格的类型系统、索引、约束、计算属性、存储过程等等。除此之外，EdgeDB
还提供许多亮眼的创新功能，比如链接属性、类型混编，以及顶尖的 JSON 支持。更多内容请参阅
[schema 文档](https://www.edgedb.com/docs/datamodel/index)。


<!-- ### Objects, not rows. ❄️ -->

<br/>
<div align="center">
  <h3>🌳 数据就是对象 🌳</h3>
</div>
<br/>

EdgeDB 彻底打破了 SQL 的束缚，从零设计了新的查询语言
EdgeQL，拥有着无与伦比的超能力：其查询结果直接就是结构化的数据对象，而不再是扁平的行列式；级联抓取相关对象易如反掌，妈妈再也不用担心我不会用 JOIN 了。

```
select 电影 {
  标题,
  演员: {
    姓名
  }
}
filter .标题 = "黑客帝国"
```

EdgeQL 的查询语句还是*模块化的*，任何 EdgeQL 查询语句都可以被当做是一个表达式，直接嵌在另一个查询语句中，这完美解决了
SQL 中恼人的*子查询*或*嵌套更新*的问题。

```
insert 电影 {
  标题 := "黑客帝国：矩阵重启",
  演员 := (
    select 人物
    filter .姓名 in {
      '基努·里维斯',
      '凯瑞-安·莫斯',
      '劳伦斯·菲什伯恩'
    }
  )
}
```

EdgeQL 远不止上面这些——它还有一个内容丰富的标准库、计算属性、多态查询、`with`
子句、事务等等很多功能，完整信息请参阅 [EdgeQL 文档](https://www.edgedb.com/docs/edgeql/index)。


<br/>
<div align="center">
  <h3>🦋 完胜 ORM 🦋</h3>
</div>
<br/>

虽然 EdgeDB 在一定程度上与 ORM 解决的是同一问题，但 EdgeDB 还能处理很多 ORM 无法解决的问题。EdgeDB
是一个完整的数据库产品，有着[正儿八经的查询语言](https://www.edgedb.com/docs/edgeql/index)、一套
[migration 系统](https://www.edgedb.com/docs/guides/migrations/index)、一整套支持不同语言的[官方客户端驱动](
https://www.edgedb.com/docs/clients/index)、一个[ CLI 命令行工具](
https://www.edgedb.com/docs/cli/index)，以及即将问世的托管云平台。EdgeDB
的使命是重新定义开发者用到的方方面面，从建模到 migration，从查询到管理数据库。

想要尝试一下新世界的开发体验吗？你只需要三个命令，就可以完成安装
CLI、启动新数据库实例，并且进入交互式 EdgeQL 查询终端。

```
$ curl --proto '=https' --tlsv1.2 -sSf https://sh.edgedb.com | sh
$ edgedb project init
$ edgedb
edgedb> select "Hello world!"
```

Windows 用户请使用下面的 Powershell 命令来安装 CLI。

```
PS> iwr https://ps1.edgedb.com -useb | iex
```

<br />

## 开始学习 EdgeDB

官方学习资源：

- **[快速上手](https://www.edgedb.com/docs/guides/quickstart)**：纯新手建议先看这篇教程，10
  分钟就可以快速完成并上手。
- **[交互式教程](https://www.edgedb.com/tutorial)**：分阶段地深入学习 EdgeQL
  查询语言，跟随教程的同时，可以在网页上直接尝试自己写的 EdgeQL 查询语句，不需要安装任何东西。
- **[电子书](https://www.edgedb.com/easy-edgedb)**：插图版《EdgeDB 
  易经》（中文版已上线！）也适合于纯新手，从最基础的概念开始，用故事穿插的方式，带你完整了解 EdgeDB 所有高深的概念。
- **官方文档**：直接读最正宗的文档，学习[建模](
  https://www.edgedb.com/docs/datamodel/index)或[编写 EdgeQL](https://www.edgedb.com/docs/edgeql/index)！

<br />


## 参与贡献

我们十分欢迎各种 PR！搭建本地 EdgeDB 开发环境，请参阅[EdgeDB 开发指南](https://www.edgedb.com/docs/internals/dev)。

[报告问题（英文） 👉](https://github.com/edgedb/edgedb/issues/new/choose)
<br />
[报告问题（中文非官方） 👉](https://gitee.com/edgedb/devrel/issues/new)
<br />
[开启讨论（英文） 👉](https://github.com/edgedb/edgedb/discussions/new)
<br />
[开启讨论（中文非官方） 👉](https://gitee.com/edgedb/devrel/issues/new)
<br />
[在线聊天（英文） 👉](https://discord.gg/umUueND6ag)

<br />

## 许可协议

本仓库中的代码遵循 Apache 2.0 许可协议进行开发与分发，详见 [LICENSE 文件](LICENSE)。
